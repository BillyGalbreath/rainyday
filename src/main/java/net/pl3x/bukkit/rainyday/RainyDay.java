package net.pl3x.bukkit.rainyday;

import net.pl3x.bukkit.rainyday.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class RainyDay extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
    }
}
