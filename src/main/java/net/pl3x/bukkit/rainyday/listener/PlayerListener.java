package net.pl3x.bukkit.rainyday.listener;

import net.pl3x.bukkit.rainyday.RainyDay;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerListener implements Listener {
    private RainyDay plugin;

    public PlayerListener(RainyDay plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        // make it rain
        Player player = event.getEntity();
        World world = player.getWorld();
        int rainTime = plugin.getConfig().getInt("rain-time", 30) * 20;
        world.setStorm(true);
        world.setWeatherDuration(rainTime);

        // make it thunder
        boolean thunder = plugin.getConfig().getBoolean("thundering", false);
        world.setThundering(thunder);
        if (thunder) {
            world.setThunderDuration(rainTime);
        }

        // set the new death message
        String deathMessage = plugin.getConfig().getString("death-message", "&e{player} has died.");
        deathMessage = deathMessage.replace("{player}", player.getDisplayName());
        event.setDeathMessage(ChatColor.translateAlternateColorCodes('&', deathMessage));
    }
}
